#ifndef TEST_AUTOMATION_HPP
#define TEST_AUTOMATION_HPP
#include <utility>
#include <optional>
#include <string>

// Utility macros for testing.

// Starts test code for a class.
// Variables:
// * control - Control variable.
// * sample - Sample variable.
#define BEGIN_TEST(classType, interface) \
{ \
classType control; \
classType sample; \
std::string interfaceStr = interface ; \
size_t failedTestCount = 0u; // Amount of tests failed.

#define TEST_COND(condition, expected_result, annotation) \
std::cout << interfaceStr << ": " << annotation; \
if (condition == expected_result) std::cout << "Received expected value \'" << #expected_result <<"\'!\n"; \
else {failedTestCount++; std::cout << "Expected "<< #expected_result << ", got "<<std::boolalpha << condition << "!\n"; }

// Ends section of test code.
// Al
#define END_TEST(buffer) \
std::cout << interfaceStr << ": " << failedTestCount << " tests failed!\n"; \
buffer.clear(); \
buffer.str(""); \
}

#endif
