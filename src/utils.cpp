#include <variant>
#include "utils.hpp"

std::regex IDrx("#(\\d+)");
std::regex NumberRx("\\d+");
// Tries to get an index/ID from string.
// Returns pair ([1] - Is ID) if successful, none otherwise.
std::optional<IntIndex> StrToIndex(const std::string& str) {
	std::smatch sm;
	// It is an ID.
	if (std::regex_match(str, sm, IDrx)) return std::make_pair(true, std::stoul(sm[1].str()));
	// It is an index.
	else if (std::regex_match(str, sm, NumberRx)) return std::make_pair(false, std::stoul(str));

	return {};
}

/*
	Insert values into a stream.
	* Argument 0 is the offset to write at.
	* Argument 1 is the byte.
	* Argument 2 and beyond are the values to insert.
	Returns true if successful, false on failure.
*/
bool insertDataFromArgs(std::basic_streambuf<char>& buf, std::deque<std::string>& args) {
	if (args.size() < 2) return false;

	if (std::all_of(args.front().cbegin(), args.front().cend(), [](int c) { return isdigit(c); }))
		buf.pubseekpos(std::stoull(args.front(), nullptr, 0u));
	else return false;

	unsigned char byteSize = 0u;
	if (std::all_of((args.begin() + 1)->cbegin(), (args.begin() + 1)->cend(), [](int c) { return isdigit(c); })) {
		byteSize = std::stoul(*(args.begin() + 1), nullptr, 0u);
	}
	else return false;
	//
	for (auto arg = args.begin() + 2; arg < args.end(); arg++) {
		std::vector<unsigned char> val(byteSize);
		if (std::all_of(arg->cbegin(), arg->cend(), [](char c) { return isdigit(c); })) {
			*reinterpret_cast<long long*>(val.data()) = std::stoll(*arg, nullptr, 0u);
		}
		else return false;

		buf.sputn(reinterpret_cast<char*>(val.data()), byteSize);
	}
	return true;
}
