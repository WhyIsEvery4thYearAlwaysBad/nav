#ifndef NAV_BASE_HPP
#define NAV_BASE_HPP
#include <map>
#include <fstream>
#include <optional>
#include <vector>
#include <tuple>
#include <any>

// Data type sizes in valve stuff.
#define VALVE_CHAR_SIZE 1
#define VALVE_SHORT_SIZE 2
#define VALVE_INT_SIZE 4
#define VALVE_FLOAT_SIZE 4
// Extra macros.
#define LATEST_NAV_MAJOR_VERSION 16 // The latest NAV major version used. (Team Fortress 2)

// An ID in NAVs is usually represented as 32-bit, 16-bit, or 8-bit
typedef unsigned int IntID;
typedef unsigned short ShortID;
typedef unsigned char ByteID;

// Cardinal direction.
enum class Direction : unsigned char {
	North = 0,
	East,
	South,
	West,
	Count
};

// Maps.
extern std::map<Direction, std::string> directionToStr;
extern std::map<std::string, Direction> strToDirection;

// Get the minimum custom area data length (in bytes) possible.
std::size_t getAreaCustomDataSize(const unsigned int& NAVVersion, const std::optional<unsigned int>& NAVSubVersion);
// Get the custom data length (in bytes) from stream.s
std::size_t getAreaCustomDataSize(std::streambuf& buf, const unsigned int& NAVVersion, const std::optional<unsigned int>& NAVSubVersion);
// Get the ideal attribute flag size (in bytes) of an area.
std::optional<unsigned char> getAreaAttributeFlagSize(const unsigned int& Version, const std::optional<unsigned int>& SubVersion);

/*
 * 0x0 - 0x3 - Identifier. (NAV Version >= 2)
 * 0x4 - 0xC - Position vector. (NAV Version >= 1)
 * 0x6 - Attribute flag. (NAV Version >= 2) */
// Class for hide spot.
class NavHideSpot {
public:
	unsigned int AreaID; // ID of the hide spot.
	std::array<float, 3> Position; // position
	unsigned char AttributeFlag; // Attributes.

	bool ReadData(std::streambuf& in, const unsigned int& NAVVersion);
	bool WriteData(std::streambuf& out, const unsigned int& NAVVersion);
	void OutputData(std::ostream& out);
};

// Approach spots
#define APPROACH_SPOT_SIZE 14 // Size of approach spots in bytes.w
class NavApproachSpot
{
public:
	IntID approachHereId = 0u, // The source ID.
	approachPrevId = 0u, approachNextId = 0u;
	unsigned char approachType = 0u, approachHow = 0u;
	// Write data to stream.
	bool WriteData(std::streambuf& out);
	bool ReadData(std::streambuf& in);
};

#define ENCOUNTER_SPOT_SIZE 5 // Total size of encounter spot data.

// NavEncounterSpot represents a spot along an encounter path
class NavEncounterSpot {
public:
	unsigned int OrderID; // The ID of the order of this spot
	unsigned char ParametricDistanceByte; // The parametric distance as a byte.
	float ParametricDistance; // Parametric Distance as a float.

	bool WriteData(std::streambuf& out);
	bool ReadData(std::streambuf& in);
  void OutputData(std::ostream& out) const;
};

#define ENCOUNTER_PATH_SIZE 10 // Total size of encounter path data.

/* Encounter paths Also called:
 * Encounter Path
 * 
*/
class NavEncounterPath {
public:
	unsigned int FromAreaID = 0u; // The ID of the area the path comes from
	Direction FromDirection = Direction::North; // The direction from the source
	unsigned int ToAreaID = 0u; // The ID of the area the path ends in
	Direction ToDirection = Direction::North; // The direction from the destination
	unsigned char spotCount = 0u;
	std::vector<NavEncounterSpot> spotContainer; // The spots along this path.
  
	bool WriteData(std::streambuf& out);
	bool ReadData(std::streambuf& in);

	// Output data.
	void OutputData(std::ostream& out);
};

#define VISIBLE_AREA_SIZE 5 // Total size of visible area data.
// Visible Area
class NavAreaBind {
public:
	unsigned int targetAreaID; // ID of the visible area
	uint8_t Attributes; // Bit-wise attributes

	bool WriteData(std::streambuf& out);
	bool ReadData(std::streambuf& in);
  void OutputData(std::ostream& out) const; // Write data in awk-friendly key-value format.
};

#define LADDER_SIZE 53 // Total size of ladder data.
// Ladder
class NavLadder {
public:
	unsigned int ID; // The ID of the ladder
	float Width; // The width of the ladder
	float Length; // The length of the ladder
	std::array<float, 3> TopVec; // The location of the center of the top of the ladder
	std::array<float, 3> BottomVec; // The location of the center of the bottom of the ladder
	unsigned int Direction; // The direction of the ladder.
	unsigned int TopForwardAreaID; // ID of the area connected to the top-forward position of the ladder
	unsigned int TopLeftAreaID; // ID of the area connected to the top-left position of the ladder
  unsigned int TopRightAreaID; // ID of the area connected to the top-right position of the ladder
	unsigned int TopBehindAreaID; // ID of area connected to the top-behind position of the ladder
	unsigned int BottomAreaID; // ID of the area connected to the bottom of the ladder

  bool ReadData(std::streambuf& in);
  bool WriteData(std::streambuf& out);
  void OutputData(std::ostream& out) const; // Write data in awk-friendly key-value format.
};

#endif
