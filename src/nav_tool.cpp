#include <iostream>
#include <iomanip>
#include <map>
#include <utility>
#include <cstring>
#include <functional>
#include <getopt.h>
#include <random>
#include <filesystem>
#include <regex>
#include <iterator>
#include <cassert>
#include <random>
#include <limits>
#include <locale>
#include <array>
#include "utils.hpp"
#include "test_automation.hpp"
#include "nav_tool.hpp"

// Program version should be defined.
#ifndef PROGRAM_VERSION
#error "PROGRAM_VERISON string macro is not defined."
#endif

// Launch options to be used by the program.
const std::array<option, 3> launch_options = {{
	{ "help", 0, nullptr, 0u }, // -h and --help
	{ "version", 0u, nullptr, 0u }, // -v and --version
	{ 0, 0, nullptr, 0 }
}};

NavTool::NavTool() {
	
}

NavTool::NavTool(int argc, char** argv) {
	// Process launch options.
	int opt;
	int long_option_index;
        while ((opt = getopt_long(argc, argv, "hiv", launch_options.data(), &long_option_index)) != -1)
	{
		switch(opt)
		{
			case 'h': // (-h) Displays concise help text.
				std::cout << "Usage: " << std::filesystem::path(argv[0]).filename().string() << " [-hiv] [--help] [--version] <structure path> <command>\n";
				return ;
			case 'i': // (-i) Writes changes to NAV data directly to the file, instead of stdout.
				edit_in_place = true;
				break;
			case 'v': // (-v) Display program version.
				std::cout << PROGRAM_VERSION << '\n';
				return ;
			case 0: // Long option.
				switch (long_option_index) {
					case 0: // (--help) Displays full help text.
						std::cout << "Usage: " << std::filesystem::path(argv[0]).filename().string() << " [-hiv][--help] [--version] <structure path> <command>\n\n\t-h, --help - Displays help text.\n\t-v, --version - Returns program version.\n\t-i - Write directly to file, instead of stdout.\n";
						break;
					case 2: // (--version) Displays program version.
						std::cout << PROGRAM_VERSION << '\n';
						break;
					default: // Unknown long launch option.
						break;
				}
				return ;
			// Invalid option.
			case '?':
			// Missing argument.
			case ':':
				exit(EXIT_FAILURE);
				break;
			default:
			break;
		}
	}
	// Parse full command.
	std::optional<ToolCmd> cmdResult = ParseCommandLine(argc, argv);
	if (cmdResult.has_value()) {
		// Can't edit in place file if said input file is stdin.
		if (edit_in_place && cmdResult.value().inFilePath.empty()) {
			std::clog << "'-i' cannot be used with stdin.\n";
			return ;
		}
		cmd = cmdResult.value();
		cmdResult.reset();
		// Execute command.
		if (!DispatchCommand(cmd)) {
			exit(EXIT_FAILURE);
		}
	}
	else exit(EXIT_FAILURE);
}

NavTool::~NavTool() {
	
}

// Mapping from cmd to ActionType
const std::map<std::string, ActionType> cmdStrToCmdType = {
	{"create", ActionType::CREATE},
	{"edit", ActionType::EDIT},
	{"delete",  ActionType::DELETE},
	{"info", ActionType::INFO},
	{"test", ActionType::TEST}
};

// Map to `TargetType` from string.
const std::map<std::string, TargetType> strToTargetType = {
	{"file", TargetType::FILE},
	{"area", TargetType::AREA},
	{"area-bind", TargetType::AREA_BIND},
	{"approach-spot", TargetType::APPROACH_SPOT},
	{"connection", TargetType::CONNECTION},
	{"encounter-path", TargetType::ENCOUNTER_PATH},
	{"encounter-spot", TargetType::ENCOUNTER_SPOT},
	{"hide-spot", TargetType::HIDE_SPOT},
	{"ladder", TargetType::LADDER}
};

// Map between string and data offset. Used for the edit command.
const std::map<std::string, size_t> strToOffset;

// Parses Command line.
// Returns ToolCmd if successful, nothing on failure.
std::optional<ToolCmd> NavTool::ParseCommandLine(unsigned int argc, char* argv[]) {
	ToolCmd cmd; unsigned int argit = 1u;
	// Cannot allow no arguments.
	if (argc <= argit) {
		std::clog << "No arguments supplied.\n";
		return {};
	}
	// Process the target data.
	auto strTargetMapIt = strToTargetType.find(std::string(argv[argit]));
	std::string argbuf = argv[argit];
	while (argit < argc && (argbuf.front() == '-' ||  strToTargetType.find(argbuf) != strToTargetType.cend()))
	{
		if (argbuf.front() == '-') {
			argbuf = argv[++argit];
			continue;
		}
		strTargetMapIt = strToTargetType.find(argbuf);
		TargetType t;
		if (strTargetMapIt == strToTargetType.cend()) break;
		else t = strTargetMapIt->second;
		// Grab param.
		switch (t)
		{
		// file <file path> | -
		case TargetType::FILE:
			// File path should be specifieds.
			if (argc <= argit + 1) {
				std::clog << "Expected file path!\n";
				return {};
			}
			// Don't set path if the argument is null. Otherwise verify the path.
			if (argv[argit + 1]) {
				// Input file path should exist.
				if (!std::filesystem::exists(argv[argit + 1])) {
					std::clog << "File \'"<<argv[argit + 1] <<"\' does not exist.\n";
					return {};
				}
				// Input file cannot be a directory.
				else if (std::filesystem::is_directory(argv[argit + 1])) {
					std::clog << "Path to file is a directory.\n";
					return {};
				}
				// Store path.
				cmd.inFilePath = argv[argit + 1];
			}
			argit += 2;
			break;
		// Target data is an area.
		case TargetType::AREA:
			{
				if (argit + 1 >= argc) {
					std::clog << "Missing area ID or index.\n";
					return {};
				}
				cmd.areaLocParam = StrToIndex(argv[argit + 1]);
				// It's invalid.
				if (!cmd.areaLocParam.has_value()) {
					std::clog << "Invalid value \'"<<argv[argit + 1]<<"\'!\n";
					return {};
				}
			}
			argit += 2u;
			break;
			// Target data is a ladder.
		case TargetType::LADDER:
			{
				if (argit + 1 >= argc) {
					std::clog << "Missing ladder ID or index.\n";
					return {};
				}
				cmd.ladderIndex = StrToIndex(argv[argit + 1]);
				// Is it invalid.
				if (!cmd.ladderIndex.has_value()) {
					std::clog << "Invalid value \'"<<argv[argit + 1]<<"\'!\n";
					return {};
				}
			}
			argit += 2u;
			break;
		// Target data is hide spot
		case TargetType::HIDE_SPOT:
		  if (!cmd.areaLocParam.has_value()) {
		    std::clog << "Area not specified.\n";
		    return {};
		  }
			if (argit + 1 >= argc) {
				std::clog << "Missing hide spot index.\n";
				return {};
			}
			cmd.hideSpotID = std::stoul(argv[argit + 1]);
			argit += 2u;
			break;
		case TargetType::ENCOUNTER_PATH:
		  if (!cmd.areaLocParam.has_value()) {
		    std::clog << "Area not specified.\n";
		    return {};
		  }
			if (argit + 1 >= argc) {
				std::clog << "Missing encounter path index.\n";
				return {};
			}
			cmd.encounterPathIndex = std::stoul(argv[argit + 1]);
			argit += 2u;
			break;
		case TargetType::ENCOUNTER_SPOT:
			// Ensure encounter path is specified
			if (cmd.target == TargetType::ENCOUNTER_PATH) {
				if (argit + 1 >= argc) {
					std::clog << "Missing encounter spot Index.\n";
					return {};
				}
				cmd.encounterSpotIndex = std::stoul(argv[argit + 1]);
			}
			else {
				std::clog << "Encounter path must be specified!\n";
				return {};
			}
			argit += 2u;
			break;
		// Connection
		case TargetType::CONNECTION:
		  if (!cmd.areaLocParam.has_value()) {
		    std::clog << "Area not specified.\n";
		    return {};
		  }
			if (argit + 1 >= argc) {
				std::clog << "Missing direction parameter.\n";
				return {};
			}
			else if (argit + 2 >= argc) {
				std::clog << "Missing connection index.\n";
				return {};
			}
			{
				Direction direc;
				// Parse direction as text or integer.
				if (std::regex_match(argv[argit + 1], std::regex("\\d+"))) {
					// Clamp value.
					direc = static_cast<Direction>(std::stoi(argv[argit + 1]));
					if (std::clamp(direc, Direction::North, Direction::West) != direc) {
						std::clog << "warning: Direction value out of range. Clamping direction value!\n";
						direc = std::clamp(direc, Direction::North, Direction::West);
					}
				}
				else {
					auto strDirecIt = strToDirection.find(argv[argit + 1]);
					if (strDirecIt == strToDirection.cend()) {
						std::clog << "Invalid direction specified!\n";
						return {};
					}
					else direc = strDirecIt->second;
				}
				// Check connection index.
				std::string buf = argv[argit + 2];
				if (!std::all_of(buf.cbegin(), buf.cend(), isxdigit)) {
					std::clog << "Invalid connection index.\n";
					return {};
				}
				cmd.connectionIndex = std::make_pair(direc, std::stoul(buf));
			}
			argit += 3u;
			break;
		case TargetType::APPROACH_SPOT:
		  if (!cmd.areaLocParam.has_value()) {
		    std::clog << "Nav area not specified.\n";
		    return {};
		  }
			if (argc < argit + 1) {
				std::clog << "Approach spot index should be specified!\n";
				return {};
			}
			// Get index if possible.
			if (std::regex_match(argv[argit + 1], std::regex("\\d+"))) cmd.approachSpotIndex = std::stoull(argv[argit + 1], nullptr, 0u);
			else {
				std::clog << "Approach spot index parameter is not a number.\n";
				return {};
			}
			argit += 2u;
			break;

		case TargetType::AREA_BIND:
		  // Area Bind can be searched for by index. Trying to search by area ID would be trouble some for duplicate binds.
		  if (!cmd.areaLocParam.has_value()) {
		    std::clog << "Area not specified!\n";
		    return {};
		  }
		  if (argc < argit + 1) {
		    std::clog << "Area-bind index not specified.\n";
		    return {};
		  }
		  cmd.areaBindIndex = StrToIndex(argv[argit + 1]);
		  if (!cmd.areaBindIndex.has_value()) {
		    std::clog << "Invalid area-bind index\n";
		    return {};
		  }
		  else if (cmd.areaBindIndex.value().first == true) {
   		    std::clog << "Area-binds can only be referred to by index.\n";
		    return {};
		  }
		  argit += 2u;
		  break;
		case TargetType::INVALID:
		default:
			std::clog << "Invalid target \'"<<argv[argit]<<"\'!\n";
			return {};
			break;
		}
		cmd.target = t;
		if (argit < argc) argbuf = argv[argit];
	}
	// Try to process action.
	if (argit < argc) {
		auto strMapIt = cmdStrToCmdType.find(std::string(argv[argit]));
		if (strMapIt != cmdStrToCmdType.cend())
		{
			cmd.cmdType = strMapIt->second;
			// Get action parameters.
			while (++argit < argc)
			{
				cmd.actionParams.emplace_back(argv[argit]);
			}
		}
	}
	// No command specified. Exit.
	else {
		std::clog << "Expected command!\n";
		return {};
	}

	// Successfully parsed command line.
	return cmd;
}

// Actually executed the command.
bool NavTool::DispatchCommand(ToolCmd& cmd) {
	// Load up input file.
	inFile.FilePath = cmd.inFilePath;
	if (cmd.cmdType != ActionType::TEST) {
		// If the file path is empty, read from stdin.
		if (inFile.FilePath.empty()) {
			if (!inFile.ReadData(*std::cin.rdbuf())) {
				std::clog << "Failed to parse NAV data from stdin. NAV data could potentially be corrupt!\n";
				return false;
			}
		}
		// Otherwise read from the file in the specified file path.
		else {
			std::ifstream inFileBuf;
			inFileBuf.open(inFile.FilePath, std::ios_base::in);
			if (!inFileBuf.is_open()) {
				std::clog << "fatal: Failed to open file buffer.\n";
				return false;
			}
			// Try to fill in file data.
			if (!inFile.ReadData(*inFileBuf.rdbuf())) {
				std::clog << "Failed to parse NAV data from file. NAV data could potentially be corrupt!\n";
				return false;
			}
		}
	}
	bool ActionSuccessful = true; // Holds whether the command successful ran.
	switch (cmd.cmdType)
	{
	// Create data.
	case ActionType::CREATE:
		ActionSuccessful = ActionCreate(cmd);
		break;
	case ActionType::EDIT:
		ActionSuccessful = ActionEdit(cmd);
		break;
	// Delete something.
	case ActionType::DELETE:
		ActionSuccessful = ActionDelete(cmd);
		break;
	// Get info of something.
	case ActionType::INFO:
		ActionSuccessful = ActionInfo(cmd);
		break;
	// Test
	case ActionType::TEST:
		ActionSuccessful = ActionTest(cmd);
		break;
	case ActionType::INVALID:
		std::clog << "Invalid command!\n";
		return false;
	default:
		break;
	}
	// Write to file if the action has sucessful ran and the action will modify NAV data.
	if (ActionSuccessful && 
	(cmd.cmdType == ActionType::CREATE
	|| cmd.cmdType == ActionType::EDIT
	|| cmd.cmdType == ActionType::DELETE)) {
		// Write to temporary buffer to ensure data is proper.
		std::stringstream dataBuf;
		if (!inFile.WriteData(*dataBuf.rdbuf())) {
			std::clog << "Failed to write NAV data.\n";
			return false;
		}
		dataBuf.seekg(0);
		// If -i is set, write directly to file.
		if (edit_in_place) {
			std::ofstream outBuf(inFile.FilePath, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
			if (outBuf.is_open()) {
				if (!outBuf.write(dataBuf.str().data(), dataBuf.str().length())) {
					std::clog << "Failed to write to file!\n";
					return false;
				}
			}
			else {
				std::clog << "Failed to open output stream.\n";
				return false;
			}
		}
		// otherwise write data to stdout.
		else std::cout.write(dataBuf.str().data(), dataBuf.str().length());
	}
	return ActionSuccessful;
}

// Executes the create action.
// Returns true if successful, false on failure.
bool NavTool::ActionCreate(ToolCmd& cmd) {
	// Ensure file can be edited.
	if (!inFile.FilePath.empty()) {
		std::filesystem::file_status inFileStatus = std::filesystem::status(inFile.FilePath);
		// Read only, can't edit.
		if (!static_cast<bool>(inFileStatus.permissions() & std::filesystem::perms::owner_write)) {
			std::clog << "Input file is read only.\n";
			return false;
		}
	}
	// Edit data.
	switch (cmd.target)
	{
	case TargetType::FILE:
		// TODO:
		return true;
		break;
	case TargetType::LADDER:
	{
		if (inFile.Version < 5u) {
		    std::clog << "NAV Data (version) is too old to contain ladder areas. (Must be at least version 5.)\n";
		    return false;
		}
		if (inFile.LadderCount < 1u) {
			std::clog << "NAV Data has no ladders\n";
			return false;
		}
		auto ladderIt = inFile.ladders.end();
		// If the index is an ID, append a new ladder with that ID.
		if (cmd.ladderIndex.value().first == false) {
			// Clamp index and ensure the ladder index parameter is within the container range.
			if (std::clamp(cmd.ladderIndex.value().second, 0u, inFile.LadderCount) != cmd.ladderIndex.value().second) {
				std::clog << "Ladder index parameter is out of range.\n";
				return false;
			}
			ladderIt = inFile.ladders.begin() + cmd.ladderIndex.value().second;
		}
		if (std::clamp(ladderIt, inFile.ladders.begin(), inFile.ladders.end()) != ladderIt) {
			std::clog << "Ladder path is out of range.\n";
			return false;
		}
		// Create ladder.
		inFile.ladders.emplace(ladderIt);
		inFile.LadderCount = inFile.ladders.size();
		std::cout << '\n';
		return true;
	}
	break;
	case TargetType::INVALID:
		return false;
	default:
		break;
	}

	if (inFile.AreaCount < 1 && cmd.target != TargetType::AREA) {
		std::clog << "Input file has no areas.\n";
		return false;
	}
	
	auto areaIt = inFile.areas.end();
	// Find the area, unless if we're creating an area.
	if (cmd.target != TargetType::AREA) {
		// Processing areas.
		assert(cmd.areaLocParam.has_value());
		// It's an ID.
		if (cmd.areaLocParam.value().first == true) {
			{
			// Check if the ID is within the minmax ID range.
			if (std::clamp(cmd.areaLocParam.value().second, inFile.cache.AreaIDRange.first, inFile.cache.AreaIDRange.second) != cmd.areaLocParam.value().second) {
				std::clog << "Requested ID is outside the found ID range #"<<std::to_string(inFile.areas.front().ID)<<" to #"<<std::to_string(inFile.areas.back().ID)<<".\n";
				return false;
			}
			// Try find the area.
			areaIt = std::find_if<std::vector<NavArea>::iterator >(inFile.areas.begin(), inFile.areas.end(), [&cmd](const NavArea& a) constexpr -> bool {
				return cmd.areaLocParam.value().second == a.ID;
			});
			}
		}
		// It's an index
		else
		{
			// Out of bounds.
			if (std::clamp(cmd.areaLocParam.value().second, 0u, inFile.AreaCount - 1u) != cmd.areaLocParam.value().second) {
				std::clog << "Specified area index is out of range.\n";
				return false;
			}
			// Set area iterator to the located area at index.
			areaIt = inFile.areas.begin() + cmd.areaLocParam.value().second;
		}

		// Could not find area iterator.
		if (std::clamp(areaIt, inFile.areas.begin(), inFile.areas.end()) != areaIt)
		{
			ERROR_MESSAGE("NavTool::ActionCreate(): Area iterator is out of range!\n", "Could not find area.\n");
			return false;
		}
	}
	// Now we edit the target data.
	switch (cmd.target) {
		case TargetType::AREA:
			// Add area.
			areaIt = inFile.areas.emplace(areaIt);
			// Set new area ID.
			if (cmd.areaLocParam.value().first == true) {
				areaIt->ID = cmd.areaLocParam.value().second;
			}
			// It's an index. Just use the area count.
			else areaIt->ID = inFile.areas.size();
			inFile.AreaCount = inFile.areas.size();
			// Set blank coords.
			areaIt->nwCorner = {0.0f, 0.0f, 0.0f};
			areaIt->seCorner = {0.0f, 0.0f, 0.0f};
			if (inFile.Version >= 11u) {
				areaIt->LightIntensity = {1.0f, 1.0f, 1.0f, 1.0f};
				if (inFile.Version >= 16u)
					areaIt->areaBindSequence.emplace(0u, std::deque<NavAreaBind>(0u));
			}
			// Fill custom data.
			areaIt->customDataSize = getAreaCustomDataSize(inFile.Version, inFile.SubVersion);
			areaIt->customData.resize(areaIt->customDataSize, 0);
			break;
		// creating hide spot.
		case TargetType::HIDE_SPOT:
		{
			// Hide spot index *should* be defined if this case is running.
			assert(cmd.hideSpotID.has_value());
			// Keep hide spot index in range.
			if (std::clamp<unsigned char>(cmd.hideSpotID.value(), 0u, areaIt->hideSpotSeq.first) != cmd.hideSpotID.value()) {
				std::clog << "Hide spot index is out of range.\n";
				return false;
			}
			// Create hide spot.
			areaIt->hideSpotSeq.second.emplace(areaIt->hideSpotSeq.second.begin() + cmd.hideSpotID.value());
			areaIt->hideSpotSeq.first = areaIt->hideSpotSeq.second.size();
		}
		break;
		// Create connection.
		case TargetType::CONNECTION:
			{
				assert(cmd.connectionIndex.has_value());
				// Validate parameters.
				if (std::clamp(cmd.connectionIndex.value().first, Direction::North, Direction::West) != cmd.connectionIndex.value().first) {
					std::clog << "Invalid direction.\n";
					return false;
				}
				// Make connection and increment connection count.
				areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.emplace(areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.begin() + cmd.connectionIndex.value().second);
				areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).first = areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.size();
			}
			break;
		
		// Create encounter path.
		case TargetType::ENCOUNTER_PATH:
		{
			assert(cmd.encounterPathIndex.has_value()); // Should be set by now.
			// Create encounter path.
			areaIt->encounterPathSequence.second.emplace(areaIt->encounterPathSequence.second.begin() + cmd.encounterPathIndex.value());
			// Increment count.
			areaIt->encounterPathSequence.first = areaIt->encounterPathSequence.second.size();
		}
		break;
		// Create encounter spot.
		case TargetType::ENCOUNTER_SPOT:
		{
			assert(cmd.encounterSpotIndex.has_value()); // Should be defined by ParseCommandLine.
			if (!cmd.encounterPathIndex.has_value()) {
				std::clog << "fatal: Encounter path index not set.\n";
				return false;
			}
			auto ePathIt = areaIt->encounterPathSequence.second.begin() + cmd.encounterPathIndex.value();
			// Create encounter spot.
			ePathIt->spotContainer.emplace(ePathIt->spotContainer.begin() + cmd.encounterSpotIndex.value());
			// Increment counter.
			ePathIt->spotCount = ePathIt->spotContainer.size();
		}
		break;
		case TargetType::APPROACH_SPOT:
			{
				assert(cmd.approachSpotIndex.has_value()); // Should be defined by ParseCommandLine.
				if (inFile.Version >= 15) {
					std::clog << "Approach spots were removed in NAV version 15.\n";
					return false;
				}
				if (std::clamp<unsigned char>(cmd.approachSpotIndex.value(), 0u, areaIt->approachSpotSequence.value().first) != cmd.approachSpotIndex.value()) {
					std::clog << "Approach spot index is out of range.\n";
					return false;
				}
				// Create approach spot.
				areaIt->approachSpotSequence.value().second.emplace(areaIt->approachSpotSequence.value().second.begin() + cmd.approachSpotIndex.value());
				// Increment counter.
				areaIt->approachSpotSequence.value().first = areaIt->approachSpotSequence.value().second.size();
			}
			break;
	case TargetType::AREA_BIND:
	  {
	    assert(cmd.areaBindIndex.has_value()); // Should already be defined.
	    if (inFile.Version < 16) {
	      std::clog << "NAV Version is under 16.\n";
	      return false;
	    }
	    auto areaBindIt = areaIt->areaBindSequence.value().second.begin() + cmd.areaBindIndex.value().second;
	    if (std::clamp(areaBindIt, areaIt->areaBindSequence.value().second.begin(), areaIt->areaBindSequence.value().second.end()) != areaBindIt) {
	      std::clog << "Area-bind index is out of range.\n";
	      return false;
	    }
	    areaIt->areaBindSequence.value().second.emplace(areaBindIt);
	    areaIt->areaBindSequence.value().first = areaIt->areaBindSequence.value().second.size();
	  }
	  break;
	  // Unset target type.
	default:
	  std::clog << "Target type is not supported.\n";
	  assert(0);
	  break;
	}

	// Let NavTool::DispatchCommand handle writing.
	return true;
}

bool NavTool::ActionEdit(ToolCmd& cmd)
{
	// There should be atleast 2 parameters.
	switch (cmd.actionParams.size()) {
		case 0:
			ERROR_MESSAGE("NavTool::ActionEdit(): No parameters.\n", "No edit parameters specified!\n");
			return false;
		case 1:
			ERROR_MESSAGE("NavTool::ActionEdit(): Atleast 2 parameters are needed. There's only one.\n", "Value unspecified\n");
			return false;
		default:
			break;
	}
	// Check mappings to ensure they actually exist.
	if (!std::all_of(cmd.actionParams.cbegin(), cmd.actionParams.cend(), [](std::string_view view) -> bool {
		// Ensure it is a number.
		if (std::all_of(view.begin(), view.end(), [](unsigned char c) -> bool {
				return std::isxdigit(c);
			})) {
			return true;
		}
		else return false;
	})) {
		return false;
	}
	// Ensure file can be written to, if there is one.
	if (!cmd.inFilePath.empty()) {
		std::filesystem::file_status inFileStatus = std::filesystem::status(inFile.FilePath);
		// No write permissions, so can't edit.
		if (!(bool)(inFileStatus.permissions() & std::filesystem::perms::owner_write)) {
			std::clog << "Input file is read only.\n";
			return false;
		}
	}
	std::stringstream dataBuf; // To store NAV data.
	// Start editing data.
	switch (cmd.target)
	{
	case TargetType::FILE:
		{
			
			return true;
		}
		break;
	case TargetType::LADDER:
	  {
	    if (inFile.Version < 5) {
	      std::clog << "NAV Data (version) is too old to contain ladder areas. (Must be at least version 5.)\n";
	      return false;
	    }
	    if (inFile.LadderCount < 1u) {
	      std::clog << "NAV Data has no ladders\n";
	      return false;
	    }
	    auto ladderIt = inFile.ladders.end();
	    // Is ID. Search for ladder based on ladder ID.
	    if (cmd.ladderIndex.value().first == true) {
	      // Verify that the Ladder ID is in the possible ladder ID range.
	      if (std::clamp(cmd.ladderIndex.value().second, inFile.cache.LadderIDRange.first, inFile.cache.LadderIDRange.second) != cmd.ladderIndex.value().second) {
		std::clog << "Could not find ladder ID #"<<std::to_string(cmd.areaLocParam.value().second)<<".\n";
		return false;
	      }
	      // Ladder ID parameter is in possible ID range. Find it.
	      ladderIt = std::find_if(inFile.ladders.begin(), inFile.ladders.end(), [&cmd](const NavLadder& ladder) {
										      return cmd.ladderIndex.value().second == ladder.ID;
										    });
	    }
	    // Ladder index parameter should be interpeted as an index.
	    else {
	      // Clamp index and ensure the ladder index parameter is within the container range.
	      if (std::clamp(cmd.ladderIndex.value().second, 0u, inFile.LadderCount - 1u) != cmd.ladderIndex.value().second) {
		std::clog << "Ladder index parameter is out of range.\n";
		return false;
	      }
	      ladderIt = inFile.ladders.begin() + cmd.ladderIndex.value().second;
	    }
	    if (std::clamp(ladderIt, inFile.ladders.begin(), inFile.ladders.end() - 1u) != ladderIt) {
	      ERROR_MESSAGE("NavTool::ActionInfo(): Ladder iterator is null!\n", "Could not find ladder!\n");
	      return false;
	    }
	    // Edit ladder data.
	    if (!ladderIt->WriteData(*dataBuf.rdbuf())) {
	      std::clog << "Failed to write ladder data.\n";
	      return false;
	    }
	    if (!insertDataFromArgs(*dataBuf.rdbuf(), cmd.actionParams)) {
	      std::clog << "Failed to edit ladder data.\n";
	      return false;
	    }
	  if (!ladderIt->ReadData(*dataBuf.rdbuf())) {
	      std::clog << "Failed to read edit ladder data.\n";
	      return false;
	    }
	    return true;
	  }
	  break;	
	default:
		break;
	}
	// Area iterator.
	auto areaIt = inFile.areas.end();
	// Processing areas.
	assert(cmd.areaLocParam.has_value());
	// Area locator parameter an ID. Need to verify the Area with the ID actually exists.
	if (cmd.areaLocParam.value().first == true) {
		if (std::clamp(cmd.areaLocParam.value().second, inFile.cache.AreaIDRange.first, inFile.cache.AreaIDRange.second) != cmd.areaLocParam.value().second) {
			std::clog << "Area ID not found.\n";
			return false;
		}
		// Validated the area ID is within the area ID range. Search for it.
		areaIt = std::find_if(inFile.areas.begin(), inFile.areas.end(), [&cmd](const NavArea& area) {
			return cmd.areaLocParam.value().second == area.ID;
		});
	}
	// Area locator parameter is an index. Simply set the iterator to the areas[index].
	else
	{
		// Out of bounds.
		if (std::clamp(cmd.areaLocParam.value().second, 0u, inFile.AreaCount) != cmd.areaLocParam.value().second) {
			std::clog << "Specified area index is out of range.\n";
			return false;
		}
		areaIt = inFile.areas.begin() + cmd.areaLocParam.value().second;
	}
	// Verify that the area iterator is actually valid.
	if (std::clamp(areaIt, inFile.areas.begin(), inFile.areas.end() - 1u) != areaIt)
	{
		std::clog << "Could not find area.\n";
		return false;
	}
	// Start editing the target data.
	switch (cmd.target) {
		case TargetType::AREA:
			{
				// Write data to buffer.
				if (!areaIt->WriteData(*dataBuf.rdbuf(), inFile.Version, inFile.SubVersion)) {
					std::clog << "Failed to store area data. Exiting.\n";
					return false;

				}
				if (!insertDataFromArgs(*dataBuf.rdbuf(), cmd.actionParams)) {
				  std::clog << "Failed to edit area data.\n";
				  return false;
				}
				dataBuf.seekg(0u);
				// Read data from buffer.
				if (!areaIt->ReadData(*dataBuf.rdbuf(), inFile.Version, inFile.SubVersion)) {
					std::clog << "Failed to get edited area data. Exiting.\n";
					return false;
				}
			}
			break;
		// Editing hide spot.
		case TargetType::HIDE_SPOT:
		{
			// Are there hiding spots in the first place?
			if (areaIt->hideSpotSeq.first < 1)
			{
				std::clog << "Area has no hide spots.\n";
				return false;
			}
			// Should not happen.
			assert(cmd.hideSpotID.has_value());
			// Is it in range.
			if (std::clamp<unsigned char>(cmd.hideSpotID.value(), 0, areaIt->hideSpotSeq.first - 1) != cmd.hideSpotID.value()) {
				std::clog << "Hide spot index parameter is out of range.\n";
				return false;
			}
			// New hide spot.
			auto hSpotIt = areaIt->hideSpotSeq.second.begin() + cmd.hideSpotID.value();
			// Validate iterator.
			assert(hSpotIt < areaIt->hideSpotSeq.second.end());
			// Write data to buffer.
			if (!hSpotIt->WriteData(*dataBuf.rdbuf(), inFile.Version)) {
				std::clog << "Failed to store hide spot data. Exiting.\n";
				return false;

			}
			if (!insertDataFromArgs(*dataBuf.rdbuf(), cmd.actionParams)) {
				std::clog << "Failed to edit hide spot arguments!\n";
				return false;
			}
			dataBuf.seekg(0u);
			// Read data from buffer.
			if (!hSpotIt->ReadData(*dataBuf.rdbuf(), inFile.Version)) {
				std::clog << "Failed to get edited hide spot data. Exiting.\n";
				return false;
			}
		}
		break;
		// Editing encounter path.
		case TargetType::ENCOUNTER_PATH:
			{
				if (areaIt->encounterPathSequence.first == 0) {
					std::clog << "Area has no encounter paths\n";
					return false;
				}
				// encounterPathIndex should already be defined.
				assert(cmd.encounterPathIndex.has_value());
				// Validate encounter path index.
				if (std::clamp(cmd.encounterPathIndex.value(), 0u, areaIt->encounterPathSequence.first - 1) != cmd.encounterPathIndex.value())
				{
					std::clog << "Invalid encounter path index.\n";
					return false;
				}
				auto ePathIt = areaIt->encounterPathSequence.second.begin() + cmd.encounterPathIndex.value();
				// Iterator should never be invalid since the index will be kept in check.
				assert(ePathIt < areaIt->encounterPathSequence.second.end());
				// Write data to buffer.
				if (!ePathIt->WriteData(*dataBuf.rdbuf())) {
					std::clog << "Failed to store area data. Exiting.\n";
					return false;

				}
				if (!insertDataFromArgs(*dataBuf.rdbuf(), cmd.actionParams)) {
					std::clog << "Invalid `nav-edit` arguments!\n";
					return false;
				}
				dataBuf.seekg(0u);
				// Read data from buffer.
				if (!ePathIt->ReadData(*dataBuf.rdbuf())) {
					std::clog << "Failed to get edited area data. Exiting.\n";
					return false;
				}
			}
			break;
		// Editing encounter spot.
		case TargetType::ENCOUNTER_SPOT:
			{
				if (!cmd.encounterPathIndex.has_value())
				{
					std::clog << "Encounter path index is undefined.\n";
					return false;
				}
				if (!cmd.encounterSpotIndex.has_value())
				{
					std::clog << "Encounter spot index is undefined.\n";
					return false;
				}
				// Validate encounter path index.
				if (std::clamp(cmd.encounterPathIndex.value(), 0u, areaIt->encounterPathSequence.first - 1) != cmd.encounterPathIndex.value())
				{
					std::clog << "Invalid encounter path index.\n";
					return false;
				}
				// Validate encounter spot index.
				if (std::clamp(cmd.encounterSpotIndex.value(), static_cast<unsigned char>(0), static_cast<unsigned char>(areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotCount - 1)) != cmd.encounterSpotIndex.value())
				{
					std::clog << "Invalid encounter spot index.\n";
					return false;
				}
				// Iterator.
				auto eSpotIt = areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotContainer.begin() + cmd.encounterSpotIndex.value();
				// Validate iterator.
				if (eSpotIt >= areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotContainer.end()) {
					std::clog << "Bad iterator to encounter spot\n";
					return false;
				}
				// Write data to buffer.
				if (!eSpotIt->WriteData(*dataBuf.rdbuf())) {
					std::clog << "Failed to store area data. Exiting.\n";
					return false;

				}
				if (!insertDataFromArgs(*dataBuf.rdbuf(), cmd.actionParams)) {
					std::clog << "Invalid `nav-edit` arguments!\n";
					return false;
				}
				dataBuf.seekg(0u);
				// Read data from buffer.
				if (!eSpotIt->ReadData(*dataBuf.rdbuf())) {
					std::clog << "Failed to get edited area data. Exiting.\n";
					return false;
				}
			}
			break;
		// Edit Connection.
		case TargetType::CONNECTION:
			{
				// Connection index should already be defined via ParseCommandLine();
				assert(cmd.connectionIndex.has_value());
				// Clamp connection index.
				if (std::clamp(cmd.connectionIndex.value().second, 0u, areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).first) != cmd.connectionIndex.value().second) {
					std::clog << "Connection index is out of range.\n";
					return false;
				}
				// Iterator to connection.
				auto connectionIt = areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.begin() + cmd.connectionIndex.value().second;
				// Validate Iterator.
				if (connectionIt >= areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.end())
				{
					std::clog << "Cannot find connection.\n";
					return false;
				}
				// Write data to buffer.
				if (!connectionIt->WriteData(*dataBuf.rdbuf())) {
					std::clog << "Failed to store connection data. Exiting.\n";
					return false;

				}
				if (!insertDataFromArgs(*dataBuf.rdbuf(), cmd.actionParams)) {
					std::clog << "Invalid `nav-edit` arguments!\n";
					return false;
				}
				dataBuf.seekg(0u);
				// Read data from buffer.
				if (!connectionIt->ReadData(*dataBuf.rdbuf())) {
					std::clog << "Failed to read edited connection data. Exiting.\n";
					return false;
				}
			}
			break;
		// Editing approach spot.
		case TargetType::APPROACH_SPOT:
			{
				// Connection index should already be defined via ParseCommandLine();
				assert(cmd.approachSpotIndex.has_value());
				// Clamp connection index.
				if (std::clamp<unsigned char>(cmd.approachSpotIndex.value(), 0u, areaIt->approachSpotSequence.value().first - 1) != cmd.approachSpotIndex.value()) {
					std::clog << "Approach spot index is out of range.\n";
					return false;
				}
				// Iterator to approach spot.
				auto aSpotIt = areaIt->approachSpotSequence.value().second.begin() + cmd.approachSpotIndex.value();
				// Validate Iterator.
				if (aSpotIt >= areaIt->approachSpotSequence.value().second.end())
				{
					std::clog << "Cannot find approach spot.\n";
					return false;
				}
				// Write approach spot data to buffer.
				if (!aSpotIt->WriteData(*dataBuf.rdbuf())) {
					std::clog << "Failed to store approach spot data. Exiting.\n";
					return false;

				}
				if (!insertDataFromArgs(*dataBuf.rdbuf(), cmd.actionParams)) {
					std::clog << "Invalid `nav-edit` arguments!\n";
					return false;
				}
				dataBuf.seekg(0u);
				// Read data from buffer.
				if (!aSpotIt->ReadData(*dataBuf.rdbuf())) {
					std::clog << "Failed to read edited approach spot data. Exiting.\n";
					return false;
				}
			}
			break;
	case TargetType::AREA_BIND:
	  {
	    assert(cmd.areaBindIndex.has_value()); // Should already be defined.
	    if (inFile.Version < 16) {
	      std::clog << "NAV Version is under 16\n";
	      return false;
	    }
	    if (areaIt->areaBindSequence.value().first < 1u) {
	      std::clog << "Area has no area-binds.\n";
	      return false;
	    }
	    auto areaBindIt = areaIt->areaBindSequence.value().second.begin() + cmd.areaBindIndex.value().second;
	    if (std::clamp(areaBindIt, areaIt->areaBindSequence.value().second.begin(), areaIt->areaBindSequence.value().second.end() - 1u) != areaBindIt) {
	      std::clog << "Area-bind index is out of range.\n";
	      return false;
	    }
	    // Edit data.
	    if (!areaBindIt->WriteData(*dataBuf.rdbuf())) {
	      std::clog << "Failed to write area-bind data\n";
	      return false;
	    }
	    if (!insertDataFromArgs(*dataBuf.rdbuf(), cmd.actionParams)) {
		std::clog << "Failed to insert data.\n";
		return false;
	    } 
	    dataBuf.seekg(0u);
	    if (!areaBindIt->ReadData(*dataBuf.rdbuf())) {
	      std::clog << "Failed to read edited data.\n";
	      return false;
	    }      
	  }
	  break;
	default:
	  std::clog << "Can't handle this type of data yet.\n";
	  return false;
	}
	// Let NavTool::DispatchCommand handle writing NAV data.
	return true;
}


bool NavTool::ActionDelete(ToolCmd& cmd)
{
	// Command should not be invalid at this point.
	assert(cmd.target != TargetType::INVALID);
	// Add another index for quick erasion of multiple structures,
	size_t endIndex = 1u;
	if (cmd.actionParams.size() > 0) {
		if (std::all_of(cmd.actionParams.front().cbegin(), cmd.actionParams.front().cend(), [](unsigned char c) { return isdigit(c); } )) {
			endIndex = std::stoull(cmd.actionParams.front(), nullptr, 0);
		}
		else {
			std::clog << "Parameter '"<< cmd.actionParams.front() <<"' is not an unsigned number.\n";
			return false;
		}
	}
	// Verify that the input file has write permissions.
	if (!cmd.inFilePath.empty()) {
		// Ensure file is modifable.
		std::filesystem::file_status inFileStatus = std::filesystem::status(inFile.FilePath);
		// Read only, can't edit.
		if (!static_cast<bool>(inFileStatus.permissions() & std::filesystem::perms::owner_write)) {
			std::clog << "Input file is read only.\n";
			return false;
		}
	}
	// Delete data.
	switch (cmd.target)
	{

	case TargetType::FILE:
		{
			// TODO:
			return true;
		}
		break;
			case TargetType::LADDER:
		  {
		    if (inFile.Version < 5) {
		      std::clog << "NAV Data (version) is too old to contain ladder areas. (Must be at least version 5.)\n";
		      return false;
		    }
		    if (inFile.LadderCount < 1u) {
		      std::clog << "NAV Data has no ladders\n";
		      return false;
		    }
		    auto ladderIt = inFile.ladders.end();
		    // Is ID. Search for ladder based on ladder ID.
		    if (cmd.ladderIndex.value().first == true) {
		      // Verify that the Ladder ID is in the possible ladder ID range.
		      if (std::clamp(cmd.ladderIndex.value().second, inFile.cache.LadderIDRange.first, inFile.cache.LadderIDRange.second) != cmd.ladderIndex.value().second) {
			std::clog << "Could not find ladder ID #"<<std::to_string(cmd.areaLocParam.value().second)<<".\n";
			return false;
		      }
		      // Ladder ID parameter is in possible ID range. Find it.
		      ladderIt = std::find_if(inFile.ladders.begin(), inFile.ladders.end(), [&cmd](const NavLadder& ladder) {
											      return cmd.ladderIndex.value().second == ladder.ID;
											    });
		    }
		    // Ladder index parameter should be interpeted as an index.
		    else {
		      // Clamp index and ensure the ladder index parameter is within the container range.
		      if (std::clamp(cmd.ladderIndex.value().second, 0u, inFile.LadderCount - 1u) != cmd.ladderIndex.value().second) {
			std::clog << "Ladder index parameter is out of range.\n";
			return false;
		      }
		      ladderIt = inFile.ladders.begin() + cmd.ladderIndex.value().second;
		    }
		    if (std::clamp(ladderIt + endIndex, inFile.ladders.begin(), inFile.ladders.end() - 1u) != ladderIt + endIndex) {
		      ERROR_MESSAGE("NavTool::ActionInfo(): Ladder iterator is null!\n", "Ladder index range falls out of range.\n");
		      return false;
		    }
		    // Erase ladder.
		    inFile.ladders.erase(ladderIt, ladderIt + endIndex);
		    inFile.LadderCount = inFile.ladders.size();
		    std::cout << '\n';
		    return true;
		  }
		  break;
	case TargetType::INVALID:
		return false;
	default:
		break;
	}
	if (inFile.AreaCount < 1u) {
		std::clog << "NAV data has no areas.\n";
		return false;
	}
	bool Valid = true;
	// Iterator to specified area.
	auto areaIt = inFile.areas.end();
	// This should be defined.
	assert(cmd.areaLocParam.has_value());
	// It's an ID. Do some checking to ensure we can reach it.
	if (cmd.areaLocParam.value().first == true) 
		{
			// Sort area container so that we can get the mins and maxes.
			// Check if the ID is within the minmax ID range.
			if (std::clamp(cmd.areaLocParam.value().second, inFile.cache.AreaIDRange.first, inFile.cache.AreaIDRange.second) != cmd.areaLocParam.value().second) {
				std::clog << "Requested ID is outside the found ID range #" << inFile.cache.AreaIDRange.first <<" to #"<< inFile.cache.AreaIDRange.second <<".\n";
				return false;
			}
			// Try find the area.
			areaIt = std::find_if<std::vector<NavArea>::iterator >(inFile.areas.begin(), inFile.areas.end(), [&cmd](const NavArea& a) constexpr -> bool {
				return cmd.areaLocParam.value().second == a.ID;
			});
		}
	// It's an index
	else
	{
		// Out of bounds.
		if (std::clamp<unsigned int>(cmd.areaLocParam.value().second, 0u, inFile.AreaCount) != cmd.areaLocParam.value().second) {
			std::clog << "Specified area index is out of range.\n";
			return false;
		}
		// Go to the area where the index points to.
		areaIt = inFile.areas.begin() + cmd.areaLocParam.value().second;
	}
	// Did we find the specified area.
	if (std::clamp(areaIt, inFile.areas.begin(), inFile.areas.end()) != areaIt) {
		ERROR_MESSAGE("NavTool::ActionDelete(): Area iterator is null / at container end!\n", "Could not find area.\n");
		return false;
	}
	// We got a valid area iterator.
	// Now we delete the target data.
	switch (cmd.target) {
		case TargetType::AREA:
			// Keep iterator in range.
			if (std::clamp(areaIt + endIndex, inFile.areas.begin(), inFile.areas.end()) != (areaIt + endIndex)) {
				std::clog << "End index is out of range.\n";
				return false;
			}
			// Delete area.
			inFile.areas.erase(areaIt, areaIt + endIndex);
			inFile.AreaCount = inFile.areas.size();
			break;
		case TargetType::CONNECTION:
			// connectionIndex should be defined if the program has managed to get here AND the command is actually targetting a connection.
			assert(cmd.connectionIndex.has_value());
			//
			if (areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).first < 1)
			{
				std::clog << "Area has no connections for direction \'"<<static_cast<unsigned char>(cmd.connectionIndex.value().first)<<"\'\n";
				Valid = false;
				break;
			}
			{
				auto connectionIt = areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.begin() + cmd.connectionIndex.value().second;
				// Validate.
				if (std::clamp(connectionIt + cmd.connectionIndex.value().second + endIndex, areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.begin(), areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.end()) != connectionIt + cmd.connectionIndex.value().second + endIndex)
				{
					std::clog << "Connection index is out of range.\n";
					Valid = false;
					break;
				}
				// Remove connection.
				areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.erase(connectionIt, connectionIt + endIndex);
				areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).first = areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.size();
			}
			break;
		case TargetType::HIDE_SPOT:
		{
			// HideSpotID should hopefully be defined here.
			// The target wouldn't be a hide spot if the parameters did not target hide spots.
			assert(cmd.hideSpotID.has_value());
			//
			if (areaIt->hideSpotSeq.first < 1u) {
				std::clog << "Area has no hide spots.\n";
				Valid = false;
				break;
			}
			{
				auto hSpotIt = areaIt->hideSpotSeq.second.begin() + cmd.hideSpotID.value();
				// Clamp
				if (std::clamp(hSpotIt + endIndex, areaIt->hideSpotSeq.second.begin(), areaIt->hideSpotSeq.second.end()) != hSpotIt + endIndex) {
					std::clog << "Hide spot index parameter is out of range.\n";
					Valid = false;
					break;
				}
				// Remove hide spot.
				areaIt->hideSpotSeq.second.erase(hSpotIt, hSpotIt + endIndex);
				areaIt->hideSpotSeq.first = areaIt->hideSpotSeq.second.size();
			}
		}
		break;
		// Remove encounter path.
		case TargetType::ENCOUNTER_PATH:
		case TargetType::ENCOUNTER_SPOT:
			{
				// Get encounter path index.
				assert(cmd.encounterPathIndex.has_value()); // Should be defined.
				if (areaIt->encounterPathSequence.first < 1u) {
					ERROR_MESSAGE("NavTool::ActionDelete(): Area has no encounter paths.\n", "Area has no encounter paths.\n");
					return false;
				}
				// Remove encounter path IF the target datum is an encounter path.
				if (cmd.target == TargetType::ENCOUNTER_PATH) {
					auto ePathIt = areaIt->encounterPathSequence.second.begin() + cmd.encounterPathIndex.value();
					if (std::clamp(ePathIt + endIndex, areaIt->encounterPathSequence.second.begin(), areaIt->encounterPathSequence.second.end()) != ePathIt + endIndex) {
						std::clog << "Encounter path index is out of range.\n";
						return false;
					}
					areaIt->encounterPathSequence.second.erase(ePathIt, ePathIt + endIndex);
					areaIt->encounterPathSequence.first = areaIt->encounterPathSequence.second.size();
				}
				// Try to remove encounter spot.
				else {
					assert(cmd.encounterSpotIndex.has_value()); // Should already be defined.
					if (areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotCount < 1u) {
						ERROR_MESSAGE("NavTool::ActionDelete(): Area has no encounter spots.\n", "Area has no encounter spots.\n");
						return false;
					}
					// Remove encounter spot.
					auto eSpotIt = areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotContainer.begin() + cmd.encounterSpotIndex.value();
					// Clamp the index.
					if (std::clamp(eSpotIt + endIndex, areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotContainer.begin(), areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotContainer.end()) != eSpotIt + endIndex) {
						std::clog << "Encounter spot index is out of range.\n";
						return false;
					}
					areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotContainer.erase(eSpotIt, eSpotIt + endIndex);
					areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotCount = areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotContainer.size();
				}
			}
			break;
		case TargetType::APPROACH_SPOT:
			{
				assert(cmd.approachSpotIndex.has_value()); // Should already be defined.
				// Verify proper verison.
				if (inFile.Version >= 15) {
					std::clog << "Approach spots have been removed since NAV Version 15.\n";
					return false;
				}
				if (areaIt->approachSpotSequence.value().first < 1) {
					std::clog << "Area has no approach spots.\n";
					return false;
				}
				// Attempt to remove approach spot.
				{
					auto aSpotIt = areaIt->approachSpotSequence.value().second.begin() + cmd.approachSpotIndex.value();
					if (std::clamp(aSpotIt + endIndex, areaIt->approachSpotSequence.value().second.begin(), areaIt->approachSpotSequence.value().second.end() - 1) != aSpotIt + endIndex) {
						std::clog << "Approach Spot index is out of range!\n";
						return false;
					}
					areaIt->approachSpotSequence.value().second.erase(aSpotIt, aSpotIt + endIndex);
					areaIt->approachSpotSequence.value().first = areaIt->approachSpotSequence.value().second.size();
				}
			}
			break;
	case TargetType::AREA_BIND:
	{
		assert(cmd.areaBindIndex.has_value()); // Should already be defined.
	    if (inFile.Version < 16) {
	      std::clog << "NAV Version is under 16\n";
	      return false;
	    }
	    if (areaIt->areaBindSequence.value().first < 1) {
	      std::clog << "Area has no area-binds.\n";
	      return false;
	    }
	    auto areaBindIt = areaIt->areaBindSequence.value().second.begin() + cmd.areaBindIndex.value().second;
	    if (std::clamp(areaBindIt + endIndex, areaIt->areaBindSequence.value().second.begin(), areaIt->areaBindSequence.value().second.end() - 1) != areaBindIt + endIndex) {
	      std::clog << "Area-bind index is out of range.\n";
	      return false;
	    }
	    areaIt->areaBindSequence.value().second.erase(areaBindIt, areaBindIt + endIndex);
	    areaIt->areaBindSequence.value().first = areaIt->areaBindSequence.value().second.size();
	  }
	  break;
		default:
			std::clog << "This target type is not supported.\n";
			Valid = false;
			break;
	}
	// Let NavTool::DispatchCommand handle writing NAV data.
	return Valid;
}

// Outputs data from a structure path.
bool NavTool::ActionInfo(ToolCmd& cmd)
{
	switch (cmd.target)
	{
		case TargetType::FILE:
		{
			inFile.OutputData(std::cout);
			std::cout << '\n';
			return true;
		}
		break;
		case TargetType::LADDER:
		  {
		    if (inFile.Version < 5) {
		      std::clog << "NAV Data (version) is too old to contain ladder areas. (Must be at least version 5.)\n";
		      return false;
		    }
		    if (inFile.LadderCount < 1u) {
		      std::clog << "NAV Data has no ladders\n";
		      return false;
		    }
		    auto ladderIt = inFile.ladders.end();
		    // Is ID. Search for ladder based on ladder ID.
		    if (cmd.ladderIndex.value().first == true) {
		      // Verify that the Ladder ID is in the possible ladder ID range.
		      if (std::clamp(cmd.ladderIndex.value().second, inFile.cache.LadderIDRange.first, inFile.cache.LadderIDRange.second) != cmd.ladderIndex.value().second) {
			std::clog << "Could not find ladder ID #"<<std::to_string(cmd.areaLocParam.value().second)<<".\n";
			return false;
		      }
		      // Ladder ID parameter is in possible ID range. Find it.
		      ladderIt = std::find_if(inFile.ladders.begin(), inFile.ladders.end(), [&cmd](const NavLadder& ladder) {
											      return cmd.ladderIndex.value().second == ladder.ID;
											    });
		    }
		    // Ladder index parameter should be interpeted as an index.
		    else {
		      // Clamp index and ensure the ladder index parameter is within the container range.
		      if (std::clamp(cmd.ladderIndex.value().second, 0u, inFile.LadderCount - 1u) != cmd.ladderIndex.value().second) {
			std::clog << "Ladder index parameter is out of range.\n";
			return false;
		      }
		      ladderIt = inFile.ladders.begin() + cmd.ladderIndex.value().second;
		    }
		    if (std::clamp(ladderIt, inFile.ladders.begin(), inFile.ladders.end() - 1u) != ladderIt) {
		      ERROR_MESSAGE("NavTool::ActionInfo(): Ladder iterator is null!\n", "Could not find ladder!\n");
		      return false;
		    }
		    // Output ladder data.
		    ladderIt->OutputData(std::cout);
		    std::cout << '\n';
		    return true;
		  }
		  break;
	default:
	  break;
	}

	// Get info of area.
	if (!cmd.areaLocParam.has_value()) {
		#ifndef NDEBUG
		std::clog << "NavTool::DispatchCommand(ToolCmd&): FATAL: Area not specified!\n";
		#endif
		return false;
	}
	// No Areas so we can't do anything.
	if (inFile.AreaCount < 1)
	{
		std::clog << "Input file has no areas.\n";
		return false;
	}
	
	auto areaIt = inFile.areas.end();
	// Is ID. Search for area.
	if (cmd.areaLocParam.value().first == true) {
		// Verify that the Area ID is in the possible area ID range.
		if (std::clamp(cmd.areaLocParam.value().second, inFile.cache.AreaIDRange.first, inFile.cache.AreaIDRange.second) != cmd.areaLocParam.value().second) {
			std::clog << "Could not find area ID #"<<std::to_string(cmd.areaLocParam.value().second)<<".\n";
			return false;
		}
		// Area ID parameter is in possible ID range. Find it.
		areaIt = std::find_if(inFile.areas.begin(), inFile.areas.end(), [&cmd](const NavArea& area) {
			return cmd.areaLocParam.value().second == area.ID;
		});
	}
	// Area locator parameter should be interpeted as an index.
	else {
		// Clamp index and ensure the area index parameter is within the container range.
		if (std::clamp(cmd.areaLocParam.value().second, 0u, inFile.AreaCount - 1) != cmd.areaLocParam.value().second) {
			std::clog << "Area index parameter is out of range.\n";
			return false;
		}
		areaIt = inFile.areas.begin() + cmd.areaLocParam.value().second;
	}
	if (std::clamp(areaIt, inFile.areas.begin(), inFile.areas.end()) != areaIt) {
		ERROR_MESSAGE("NavTool::ActionInfo(): Area iterator is null!\n", "Could not find area!\n");
		return false;
	}
	// Output
	switch (cmd.target)
	{
	case TargetType::AREA:
		{
			areaIt->OutputData(std::cout);
			// Also output custom data if there is custom data.
			if (areaIt->customDataSize > 0) {
				switch (inFile.SubVersion.value())
				{
				// CS:S and CS:GO stores approach spots in the custom data section.
				case 1u:
					{
					  std::cout << "custom_data: csgo\n";
						if (inFile.SubVersion == 1) {
							unsigned char ApproachSpotCount = *areaIt->customData.begin();
							std::cout << "approach_spot_count: " << std::to_string(ApproachSpotCount);
						}
					}
					break;
				// Get TF2-specific attributes flag.
				case 2u:
				  std::cout << "custom_data: tf2\n";
					{
						unsigned int TFAttributes;
						std::copy(areaIt->customData.begin(), areaIt->customData.begin() + 3, &TFAttributes);
						std::cout << "tf2_attribute_flag: " << TFAttributes;
					}
					break;
				// Generic custom data.
				case 0u:
				  std::cout << "custom_data: none";
					break;
				// Unidentifiable NAV subversion.
				default:
					std::cout << "custom_data: unknown";
					break;
				}
			}
			// Newline.
			std::cout << '\n';
		}
		break;
	// Output hide spot data.
	case TargetType::HIDE_SPOT:
		{
			// hideSpotID should be defined in this case.
			assert(cmd.hideSpotID.has_value());
			// Empty hide spot data.
			if (areaIt->hideSpotSeq.first < 1) {
				std::clog << "Area #" << areaIt->ID << " has 0 hide spots.\n";
				return false;
			}
			// hideSpot ID is higher than hide spot count.
			if (std::clamp(cmd.hideSpotID.value(), static_cast<unsigned char>(0u), areaIt->hideSpotSeq.first) != cmd.hideSpotID.value()) {
				std::clog << "Hide spot index \'" << std::to_string(cmd.hideSpotID.value()) << "\' is out of range.\n";
				return false;
			}
			// Get hide spot data.
			auto hSpotIt = areaIt->hideSpotSeq.second.begin() + cmd.hideSpotID.value();
			// Output ID.
			hSpotIt->OutputData(std::cout);
			std::cout << '\n';
		}
		break;
	case TargetType::CONNECTION:
		{
			// Connection index should be defined.
			assert(cmd.connectionIndex.has_value());
			// Proper direction value should already be enforced by ParseCommandLine().
			assert(std::clamp(cmd.connectionIndex.value().first, Direction::North, Direction::West) == cmd.connectionIndex.value().first);
			// Is there a connection to begin with?
			if (areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).first < 1)
			{
			  std::clog << "Area has no "<<directionToStr[cmd.connectionIndex.value().first]<<" connections.\n";
				return false;
			}
			// Keep connection index in range.
			if (std::clamp(cmd.connectionIndex.value().second, 0u, areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).first - 1) != cmd.connectionIndex.value().second) {
				std::cout << "Connection index \'"<< cmd.connectionIndex.value().second<< "\' is out of range!\n";
				return false;
			}
			// Get iterator.
			auto connectionIt = areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.begin() + cmd.connectionIndex.value().second;
			// Iterator SHOULD be valid now.
			assert(connectionIt < areaIt->connectionData.at(static_cast<unsigned char>(cmd.connectionIndex.value().first)).second.end());
			// Output connection data.
			std::cout << "target_area_id: " << connectionIt->TargetAreaID << '\n';
		}
		break;
	// Get encounter path/spot data.
	case TargetType::ENCOUNTER_PATH:
	case TargetType::ENCOUNTER_SPOT:
		{
			// encounterPathIndex should already be defined. Checks for lack of the encounter path locator parameter is done in ParseCommandLine()
			assert(cmd.encounterPathIndex.has_value());
			// Area has no encounter paths
			if (areaIt->encounterPathSequence.first < 1u) {
				std::clog << "Area #" << areaIt->ID << " has 0 encounter paths.\n";
				return false;
			}
			// Validate encounter path index.
			if (std::max(cmd.encounterPathIndex.value(), areaIt->encounterPathSequence.first) <= cmd.encounterPathIndex.value()) {
				std::clog << "Encounter path index is out of range.\n";
				return false;
			}
			if (cmd.target == TargetType::ENCOUNTER_PATH)
			{
				areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).OutputData(std::cout);
				std::cout << '\n';
			}
			// Output encounter spot data.
			else if (cmd.target == TargetType::ENCOUNTER_SPOT) {
				// encounterSpotIndex should be defined.
				assert(cmd.encounterSpotIndex.has_value());
				// Keep encounter spot index in range.
				if (std::max(cmd.encounterSpotIndex.value(), areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotCount) <= cmd.encounterSpotIndex.value()) {
					std::clog << "Encounter Spot index is out of range.\n";
					return false;
				}
				auto eSpotIt = areaIt->encounterPathSequence.second.at(cmd.encounterPathIndex.value()).spotContainer.begin() + cmd.encounterSpotIndex.value();
				eSpotIt->OutputData(std::cout);
				std::cout << '\n';
			}
		}
		break;

	case TargetType::APPROACH_SPOT:
		{
			// Check version.
			if (inFile.Version >= 15) {
				std::clog << "This file is too new to contain approach spots.\n";
				return false;
			}
			// Ensure the area has an approach spot.
			if (areaIt->approachSpotSequence.value().first < 1)
			{
				std::clog << "Area has no approach spots.\n";
				return false;
			}
			// Approach Spot index should be defined.
			assert(cmd.approachSpotIndex.has_value());
			// Clamp approach spot.
			if (std::max(cmd.approachSpotIndex.value(), static_cast<unsigned char>(areaIt->approachSpotSequence.value().first - 1)) != cmd.approachSpotIndex.value()) {
				std::clog << "Approach spot index is out of range.\n";
				return false;
			}
			//
			auto aSpotIt = areaIt->approachSpotSequence.value().second.begin() + cmd.approachSpotIndex.value();
			// Approach spot iterator should be valid here.
			assert(aSpotIt < areaIt->approachSpotSequence.value().second.end());
			// Output.
			std::cout << "approach_type: " << std::to_string(aSpotIt->approachType)
				  << "\napproach_method:" << std::to_string(aSpotIt->approachHow)
				  << "\nprevious_area_id: " << std::to_string(aSpotIt->approachPrevId)
				  << "\nhere_area_id: " << std::to_string(aSpotIt->approachHereId)
				  << "\nnext_area_id: " << std::to_string(aSpotIt->approachNextId) << '\n';
		}
		break;
        case TargetType::AREA_BIND:
	  {
	    assert(cmd.areaBindIndex.has_value()); // Should already be defined.
	    if (inFile.Version < 16) {
	      std::clog << "NAV Version is under 16\n";
	      return false;
	    }
	    if (areaIt->areaBindSequence.value().first < 1) {
	      std::clog << "Area has no area-binds.\n";
	      return false;
	    }
	    auto areaBindIt = areaIt->areaBindSequence.value().second.begin() + cmd.areaBindIndex.value().second;
	    if (std::clamp(areaBindIt, areaIt->areaBindSequence.value().second.begin(), areaIt->areaBindSequence.value().second.end() - 1) != areaBindIt) {
	      std::clog << "Area-bind index is out of range.\n";
	      return false;
	    }
	    // Output information.
	    areaBindIt->OutputData(std::cout);
	    std::cout << '\n';
	  }
	  break;
	case TargetType::LADDER:
		// TODO:
		break;
	default:
		break;
	}
	return true;
}

// Run unit testing.
bool NavTool::ActionTest(ToolCmd& cmd) {
	unsigned int totalFailedTestCount = 0u;
	/*
	 * Utilize randomness to prevent margin-of-error from interfering with tests.
	 */
	std::mt19937 mt;
	std::uniform_int_distribution<unsigned char> byteDistrib(0u);
	std::uniform_int_distribution<int> intDistrib(0u);
	std::uniform_real_distribution<float> floatDistrib(std::numeric_limits<float>::min());
	// Buffer for binary data.
	std::stringstream dataBuf;
	// Test Nav Connection I/O
	std::cout << "---Nav Connection I/O---\n";
	BEGIN_TEST(NavConnection, "Connection I/O")
	control.TargetAreaID = mt();
	// NavConnection should write NAV data successfully.
	TEST_COND(control.WriteData(*dataBuf.rdbuf()), true, "Writing: ")
	// NavConnection should read the NAV data successfully.
	TEST_COND(sample.ReadData(*dataBuf.rdbuf()), true, "Reading: ")
	// NavConnection should have only written an integer (4 bytes)
	TEST_COND(dataBuf.str().length(), 4, "Written 4 bytes: ")
	// Control and sample should have the same values.
	TEST_COND(sample.TargetAreaID, control.TargetAreaID, "Matching data: ")
	// The ReadData function should fail (return false) if there is not enough bytes to take in.
	dataBuf.str("fas");
	TEST_COND(sample.ReadData(*dataBuf.rdbuf()), false, "Reading garbage data: ")
	END_TEST(dataBuf)
	/* Now test hide spot I/O.
	 - WriteData should write exactly 12 bytes in NAV version 1, and 17 bytes in NAV version 2 and latter.
	*/
	std::cout << "---Hide Spot I/O---\n";
	for (unsigned int version = 1u; version <= 2u; version++) {
		BEGIN_TEST(NavHideSpot, "Hide Spot I/O (Version "+std::to_string(version)+")")
		control.AreaID = mt();
		std::generate(control.Position.begin(), control.Position.end(), [&mt, &floatDistrib]() -> float {
			return floatDistrib(mt);
		});
		control.AttributeFlag = byteDistrib(mt);
		// WriteData should correctly
		TEST_COND(control.WriteData(*dataBuf.rdbuf(), version), true, "Writing: ")
		/* Should have written 12 bytes
			* 4 bytes for ID (only in NAVVersion 2)
			* 12 bytes for position float vector (only if NAV Version >= 1)
			* 1 byte for attribute flag.
		*/
		size_t expectedSize = 0u;
		switch (version) {
			case 0:
				break;
			case 1:
				expectedSize = VALVE_FLOAT_SIZE * 3;
				break;
			case 2:
				expectedSize = VALVE_INT_SIZE + (VALVE_FLOAT_SIZE * 3) + VALVE_CHAR_SIZE;
				break;
		}
		TEST_COND(dataBuf.str().length(), expectedSize, "Byte length (of written data): ")
		// ReadData should be successful here as well, if WriteData was successful.
		dataBuf.seekg(0);
		TEST_COND(sample.ReadData(*dataBuf.rdbuf(), version), true, "Reading: ");
		{
			std::stringstream sampleData;
			// Sample should be able to write good data.
			TEST_COND(sample.WriteData(*sampleData.rdbuf(), version), true, "Rewriting: ")
			// Control and sample hide spots should have the same value.
			TEST_COND(static_cast<bool>(dataBuf.str() == sampleData.str()), true, "Equal data: ")
		}
		// Garbage data. Should fail.
		dataBuf.str("asafssff");
		dataBuf.seekg(0u);
		// ReadData should fail here.
		TEST_COND(sample.ReadData(*dataBuf.rdbuf(), version), false, "Reading Garbage Data: ")
		totalFailedTestCount += failedTestCount;
		END_TEST(dataBuf)
	}
	// Test Approach Spot I/O
	std::cout << "---Approach Spot I/O---\n";
	BEGIN_TEST(NavApproachSpot, "Approach Spot I/O")
	TEST_COND(control.WriteData(*dataBuf.rdbuf()), true, "Writing uninitialized: ")
	TEST_COND(sample.ReadData(*dataBuf.rdbuf()), true, "Reading unintialized: ")
	control.approachHereId = mt();
	control.approachNextId = mt();
	control.approachPrevId = mt();
	control.approachType = byteDistrib(mt);
	control.approachHow = byteDistrib(mt);
	// Test writing with set data.
	TEST_COND(control.WriteData(*dataBuf.rdbuf()), true, "Writing initialized: ")
	TEST_COND(sample.ReadData(*dataBuf.rdbuf()), true, "Reading intialized: ")
	// Test reading garbage data.
	dataBuf.str("asfasafs");
	TEST_COND(sample.ReadData(*dataBuf.rdbuf()), false, "Reading Garbage Data: ")
	totalFailedTestCount += failedTestCount;
	END_TEST(dataBuf)
	// Test Encounter Spot I/O
	std::cout << "---Encounter Spot I/O---\n";

	BEGIN_TEST(NavEncounterSpot, "Encounter Spot I/O")
	control.OrderID = mt();
	control.ParametricDistanceByte = byteDistrib(mt);
	// WriteData should pass.
	TEST_COND(control.WriteData(*dataBuf.rdbuf()), true, "Writing: ")
	/* 5 bytes (integer + byte) should be written
		* 4 bytes for ID
		* 1 byte for distance.
	*/
	TEST_COND(dataBuf.str().length(), ENCOUNTER_SPOT_SIZE, "Written proper amount of bytes: ")
	// ReadData should be successful.
	TEST_COND(sample.ReadData(*dataBuf.rdbuf()), true, "Reading: ")
	// Ensure control and sample Encounter Spots are equal.
	TEST_COND(static_cast<bool>(control.OrderID == sample.OrderID), true, "Matching OrderIDs: ")
	TEST_COND(static_cast<bool>(control.ParametricDistanceByte == sample.ParametricDistanceByte), true, "Matching bytes: ")
	totalFailedTestCount += failedTestCount;
	END_TEST(dataBuf)
	/*
	 * Test if:
		Encounter Path binary data can be properly written and read.
	*/
	std::cout << "---Encounter Path I/O---\n";
	BEGIN_TEST(NavEncounterPath, "Encounter Path I/O")
	control.FromAreaID = mt();
	control.ToAreaID = mt();
	{
		std::uniform_int_distribution<unsigned char> directionDist(0u, static_cast<unsigned char>(Direction::Count));
		control.FromDirection = static_cast<Direction>(directionDist(mt));
		control.ToDirection = static_cast<Direction>(directionDist(mt));
	}
	control.spotCount = mt();
	control.spotContainer.resize(control.spotCount);
	std::fill(control.spotContainer.begin(), control.spotContainer.end(), NavEncounterSpot());
	// Test writing.
	TEST_COND(control.WriteData(*dataBuf.rdbuf()), true, "Writing: ")
	// Encounter Path should have at least 11 bytes.
	TEST_COND(dataBuf.str().length(), ((VALVE_INT_SIZE + VALVE_CHAR_SIZE) * 2) + VALVE_CHAR_SIZE + (control.spotCount * ENCOUNTER_SPOT_SIZE), "Written bytes: ")
	// Test reading.
	dataBuf.seekg(0u);
	TEST_COND(sample.ReadData(*dataBuf.rdbuf()), true, "Reading: ")
	// Control and Sample variables should have the same NAV data.
	{
		std::stringstream sampleDataBuf;
		TEST_COND(sample.WriteData(*sampleDataBuf.rdbuf()), true, "Rewriting: ")
		TEST_COND(static_cast<bool>(dataBuf.str() == sampleDataBuf.str()), true, "Matching bytes: ")
	}
	totalFailedTestCount += failedTestCount;
	END_TEST(dataBuf)
	  // Test area-bind I/O
	  BEGIN_TEST(NavAreaBind, "Area-Bind I/O")
	  TEST_COND(control.WriteData(*dataBuf.rdbuf()), true, "Writing: ")
	  TEST_COND(sample.ReadData(*dataBuf.rdbuf()), true, "Reading: ")
	  {
	    std::stringstream sampleDataBuf;
	    TEST_COND(sample.WriteData(*sampleDataBuf.rdbuf()), true, "Rewriting: ")
	    TEST_COND(static_cast<bool>(dataBuf.str() == sampleDataBuf.str()), true, "Matching bytes: ")
	  }
	  totalFailedTestCount += failedTestCount;
	  END_TEST(dataBuf)
	  // Test ladder I/O
	  std::cout << "---Ladder I/O---\n";
	  BEGIN_TEST(NavLadder, "Ladder I/O")
	  TEST_COND(control.WriteData(*dataBuf.rdbuf()), true, "Writing: ")
	  TEST_COND(sample.ReadData(*dataBuf.rdbuf()), true, "Reading: ")
	  {
	    std::stringstream sampleDataBuf;
	    TEST_COND(sample.WriteData(*sampleDataBuf.rdbuf()), true, "Rewriting: ")
	    TEST_COND(static_cast<bool>(dataBuf.str() == sampleDataBuf.str()), true, "Matching byte: ")
	  }
	  totalFailedTestCount += failedTestCount;
	  END_TEST(dataBuf)
	// Test Area I/O.
	// To be finished at some point.
	std::cout << "---Nav Area I/O---\n";
	for (unsigned int version = 0u; version <= 16u; version++) {
		// Set data that has always existed in the NAV format.
		BEGIN_TEST(NavArea, "Nav Area I/O (Version "+std::to_string(version)+")")
		control.ID = mt();
		control.Flags = byteDistrib(mt);
		std::generate(control.nwCorner.begin(), control.nwCorner.end(), std::bind(floatDistrib, mt));
		std::generate(control.seCorner.begin(), control.seCorner.end(), std::bind(floatDistrib, mt));
		control.NorthEastZ = control.nwCorner.at(2);
		control.SouthWestZ = control.seCorner.at(2);
		{
			std::uniform_int_distribution<unsigned char> countLimit(0, 128);
			control.encounterPathSequence.first = countLimit(mt);
			control.encounterPathSequence.second.resize(control.encounterPathSequence.first);
		}
		for (unsigned int subversion = 0u; subversion <= 2u; subversion++) {
			std::cout << "-----Subversion "<<subversion<<"-----\n";
			// Subversions!
			switch (subversion) {
				case 0:
					break;
				// CSGO Approach spots.
				case 1:
					{
						control.customDataSize = 1u;
						control.customData = { 0u };
					}
					break;
				// TF2 Attribute flag
				case 2:
					control.customDataSize = 4u;
					control.customData = {byteDistrib(mt), byteDistrib(mt), byteDistrib(mt), byteDistrib(mt)};
					break;
				// TODO: Left 4 Dead
				default:
					break;
			}
			// If area data container is undefined, function should fail.
			if (version < 15u && !control.approachSpotSequence.has_value()) {
				TEST_COND(control.WriteData(*dataBuf.rdbuf(), version, subversion), false, "Writing (with undefined approach spot container): ")
				control.approachSpotSequence.emplace();
				control.approachSpotSequence.value().first = byteDistrib(mt);
				control.approachSpotSequence.value().second.resize(control.approachSpotSequence.value().first);
				dataBuf.str("");
			}
			// Write function should fail if the ladder sequence does not exist.
			if (version >= 7u && !control.ladderSeq.has_value()) {
			  TEST_COND(control.WriteData(*dataBuf.rdbuf(), version, subversion), false, "Writing (with undefined ladder sequence): ")
			    control.ladderSeq.emplace();
			  std::for_each(control.ladderSeq.value().begin(), control.ladderSeq.value().end(), [&control, &byteDistrib, &mt](auto& pair) {
													      pair.first = byteDistrib(mt);
													      pair.second.resize(pair.first);
													    });
			}
			// If LightIntensity is undefined, then the write function should fail.
			if (version >= 11u && !control.LightIntensity.has_value()) {
				TEST_COND(control.WriteData(*dataBuf.rdbuf(), version, subversion), false, "Writing (with undefined light intensity values): ")
				control.LightIntensity = { 0.0f, 1.0f, 1.0f, 0.0f };
				dataBuf.str("");
			}
			// Write function should fail if area-bind sequence is not defined when it should be.
			if (version >= 16u && !control.areaBindSequence.has_value()) {
				TEST_COND(control.WriteData(*dataBuf.rdbuf(), version, subversion), false, "Writing (with undefined area-bind sequence): ")
				control.areaBindSequence.emplace();
				{
					std::uniform_int_distribution<unsigned int> dist(0u, 256u);
					control.areaBindSequence.value().first = dist(mt);
					control.areaBindSequence.value().second.resize(control.areaBindSequence.value().first);
				}
				dataBuf.str("");
			}
			dataBuf.seekp(0u);
			// Should succeed.
			TEST_COND(control.WriteData(*dataBuf.rdbuf(), version, subversion), true, "Writing: ")
			// Test Reading.
			dataBuf.seekg(0u);
			dataBuf.flush();
			TEST_COND(sample.ReadData(*dataBuf.rdbuf(), version, subversion), true, "Reading: ")
			// Both should have the same data.
			{
				std::stringstream sampleDataBuf;
				TEST_COND(sample.WriteData(*sampleDataBuf.rdbuf(), version, subversion), true, "Rewriting: ")
				TEST_COND(static_cast<bool>(dataBuf.str() == sampleDataBuf.str()), true, "Matching file data: ")
				sampleDataBuf.flush();
			}
			// Clear stringstream.
			dataBuf.str("");
		}
		totalFailedTestCount += failedTestCount;
		END_TEST(dataBuf)
	}
	// Test parameter parsing.
	ToolCmd testCmd;
	char** testArgs = {};
	if (ParseCommandLine(0u, testArgs).has_value()) {
		std::clog << "NavTool::ParseCommandLine() with no parameters: Expected no return object, got object.\n";
		totalFailedTestCount += 1u;
		return false;
	}
	else std::cout << "NavTool::ParseCommandLine() with no parameters: Returned with no object as expected.\n";
	std::cout << "--- Total Failed Tests: " << totalFailedTestCount << '\n';
	return totalFailedTestCount == 0u;
}

int main(int argc, char **argv) {
	NavTool navApp(argc, argv);
	// Remove temporary files.
	std::filesystem::remove_all(std::filesystem::temp_directory_path().append("nav/"));
	// Flush standard streams.
	std::clog.flush();
	std::cin.sync();
	std::cout.flush();
	exit(EXIT_SUCCESS);
}
