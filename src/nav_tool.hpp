#ifndef NAV_TOOL_HPP
#define NAV_TOOL_HPP
#include <vector>
#include "utils.hpp"
#include "nav_base.hpp"
#include "nav_file.hpp"
// Type of command.
enum class ActionType {
	CREATE, // Create data.
	EDIT, // Edit data.
	DELETE, // Delete data.
	INFO, // Display info of data.
	TEST, // Test program.
	// I want to add nav_analyze into the program, but that's too heavy handed for me currently.
	// ANALYZE, // Analyzes mesh.

	/* These commands are redundant, as they can be emulated by the above commands.
		CONNECT, // Connects two areas/ladder. (Can be emulated with `nav <path to connection> create` and `nav <path to connection> edit id 5`)
		DISCONNECT, // Disconnects two areas/ladder. (Simply remove a connection.)
		COMPRESS // Sets area IDs to iota. (Use a shell for-loop with `nav <area> edit`) */

	COUNT, // Amount of actions.
	INVALID // Invalid action type.
};

// The type of data to apply.
enum class TargetType {
	FILE, // File/stream.
	AREA, // Area
	LADDER, // Ladder
	CONNECTION, // Connection
	ENCOUNTER_PATH, // Encounter path
	ENCOUNTER_SPOT, // Encounter spot
	HIDE_SPOT, // Hide spot
	APPROACH_SPOT, // Approach Spot
	AREA_BIND, // Area bind.

	COUNT, // Amount of target types.
	INVALID // Invalid target type.
};

struct ToolCmd {
	ActionType cmdType = ActionType::INVALID; // Command to execute.
	TargetType target = TargetType::FILE; // Default target is standard input;
	std::filesystem::path inFilePath; // Path to file container.
	// IDs for type of data.
	std::optional<IntIndex> areaLocParam;
	std::optional<IntID> encounterPathIndex;
	// Index to connection,
	std::optional<std::pair<Direction, IntID> > connectionIndex;
	std::optional<ByteID> encounterSpotIndex, hideSpotID, approachSpotIndex;
  std::optional<IntIndex> areaBindIndex;
  std::optional<IntIndex> ladderIndex;
	// Parameters for the action.
	std::optional<size_t> RequestedIndex;
	std::deque<std::string> actionParams;
};

class NavTool {
	private:
		ToolCmd cmd; // Command to execute.
		NavFile inFile; // Input NAV file (or stream as file).
		std::string CommandLine;
		// Launch options
		bool edit_in_place = false; // Determines whether to write directly to nav file.
	public:
		NavTool();
		NavTool(int argc, char** argv);
		~NavTool();

	std::optional<ToolCmd>ParseCommandLine(unsigned int argc, char **argv);
	bool DispatchCommand(ToolCmd& cmd);
	// Executes the create action.
	// True if successful
	// Crate action.
	bool ActionCreate(ToolCmd& cmd);
	// Edit action.
	bool ActionEdit(ToolCmd& cmd);
	// Delete action.
	bool ActionDelete(ToolCmd& cmd);
	// Info action.
	bool ActionInfo(ToolCmd& cmd);
	// Test action.
	bool ActionTest(ToolCmd& cmd);
};
#endif
