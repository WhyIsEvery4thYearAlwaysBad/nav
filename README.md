# source-engine-nav-tool / nav

A **work-in-progress** command-line tool for modifying Source Engine NAV files. 

**NOTE: I am only able to test NAV files built by Team Fortress 2 in game.** I typically work off already existing parsers for NAVs for other games.

Also backup your NAV before trying out this tool.

The manual pages are not completely synced with the program functions yet.

## Building
Run `make` to compile the program. nav is a standalone tool, so it can be ran from any file location. 

On Unix systems that use Filesystem Hierarchy Standard, run `make install` to install programs and man pages to /usr/local.

Run `make man-pages` to build the man-pages.

## Documentation
Documentation is in the man pages located at `docs/man-pages`.
